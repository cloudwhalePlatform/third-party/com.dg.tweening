﻿using UnityEngine;
using DG.Tweening;
using DG.Tweening.Core.Enums;

public class DOTweenSetup : MonoBehaviour
{
#pragma warning disable CS0414, CS0649 // Never used & never assigned warning (unity)
    [SerializeField] bool _applyOnAwake = true;

    [Header("General")]
    [SerializeField] LogBehaviour _logBehaviour = LogBehaviour.ErrorsOnly;
    [SerializeField] float _maxSmoothUnscaledTime = .15f;
    [SerializeField] NestedTweenFailureBehaviour _nestedTweenFailureBehaviour = NestedTweenFailureBehaviour.TryToPreserveSequence;
    [SerializeField] bool _showUnityEditorReport = false;
    [SerializeField] float _timeScale = 1f;
    [SerializeField] bool _useSafeMode = true;
    [SerializeField] bool _useSmoothDeltaTime = false;
    [SerializeField] int _maxTweeners = 200;
    [SerializeField] int _maxSequences = 50;

    [Header("On Create")]
    [SerializeField] bool _defaultAutoKill = true;
    [SerializeField] AutoPlay _defaultAutoPlay = AutoPlay.All;
    [SerializeField] float _defaultEaseOvershootOrAmplitude = 1.70158f;
    [SerializeField] float _defaultEasePeriod = 0;
    [SerializeField] Ease _defaultEaseType = Ease.OutQuad;
    [SerializeField] LoopType _defaultLoopType = LoopType.Restart;
    [SerializeField] bool _defaultRecyclable = false;
    [SerializeField] bool _defaultTimeScaleIndependent = false;
    [SerializeField] UpdateType _defaultUpdateType = UpdateType.Normal;
#pragma warning restore CS0414, CS0649 // Never used & never assigned warning (unity)

    public void Awake()
    {
        if (_applyOnAwake)
        {
            ApplyConfig();
        }
    }

    public void ApplyConfig ()
    {
        DOTween.logBehaviour = _logBehaviour;
        DOTween.maxSmoothUnscaledTime = _maxSmoothUnscaledTime;
        DOTween.nestedTweenFailureBehaviour = _nestedTweenFailureBehaviour;
        DOTween.showUnityEditorReport = _showUnityEditorReport;
        DOTween.timeScale = _timeScale;
        DOTween.useSafeMode = _useSafeMode;
        DOTween.SetTweensCapacity(_maxTweeners, _maxSequences);

        DOTween.defaultAutoKill = _defaultAutoKill;
        DOTween.defaultAutoPlay = _defaultAutoPlay;
        DOTween.defaultEaseOvershootOrAmplitude = _defaultEaseOvershootOrAmplitude;
        DOTween.defaultEasePeriod = _defaultEasePeriod;
        DOTween.defaultEaseType = _defaultEaseType;
        DOTween.defaultLoopType = _defaultLoopType;
        DOTween.defaultRecyclable = _defaultRecyclable;
        DOTween.defaultTimeScaleIndependent = _defaultTimeScaleIndependent;
        DOTween.defaultUpdateType = _defaultUpdateType;
    }
}
